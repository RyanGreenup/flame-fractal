use plotters::prelude::*;
use rand::{Rng, SeedableRng};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    make_plot()?;

    Ok(())
}

fn make_plot() -> Result<(), Box<dyn std::error::Error>> {
    let root = BitMapBackend::new("output.png", (3840, 2160)).into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("", ("sans-serif", 50).into_font())
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(-1f32..1f32, -0.1f32..1f32)?;

    // Make Data
    // About 10 fold faster on release compile
    // NOTE choose a number of points in proportion to the the resolution
    let data: Vec<(f32, f32)> = make_points(10_000_000); // NOTE change number of points here


    // Add points
    chart.draw_series(
        data.iter()
            // Note the dereferencing
            .map(|(x, y)| Circle::new((*x, *y), 2, BLUE.filled())),
    )?;

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    root.present()?;

    Ok(())
}

fn make_points(n: i32) -> Vec<(f32, f32)> {
    // Pick a random number
    let mut x: f32 = rand::random();
    let mut y: f32 = rand::random();

    // make a vector of random functions
    let mut functions: Vec<fn(f32, f32) -> (f32, f32)> = Vec::new();

    functions.push(|x, y| (x.sin(), y.cos()));
    functions.push(|x, y| (x / 2.0, (y + 1.0) / 2.0));
    functions.push(|x, y| ((x + 1.0) / 2.0, y / 2.0));

    // pick a random function between 1 and 3
    let random_func = || {
        let j = rand::thread_rng().gen_range(0..functions.len());
        functions[j]
    };

    // Generate some data using those functions randomly selected
    let mut data: Vec<(f32, f32)> = Vec::new();
    let n = n as usize;
    for _ in 0..n {
        let f = random_func();
        (x, y) = f(x, y);
        data.push((x, y));
    }

    return data;
}
