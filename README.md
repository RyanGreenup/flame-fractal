# flame-fractal

Playing around with [fractal flames](https://flam3.com/flame_draves.pdf).

See [The Fractal Flame Algorithm](./flame_draves.pdf) [^1]

```r
funcs <- list(
    function(x, y) c(sin(x), cos(y)),
    function(x, y) c((x + 1) / 2, y / 2),
    function(x, y) c(x / 2, (y + 1) / 2)
)
```


![](./chaos_game_example.png)


## Footnotes

[^1]: Available from https://flam3.com/flame_draves.pdf cited in https://en.wikipedia.org/wiki/Fractal_flame
